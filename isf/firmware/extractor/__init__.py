from isf.core import logger
import binwalk
import os
from isf.firmware.extractor.resources.cpu_rec.cpu_rec import which_arch,isf_which_arch


class Extractor:
    bin_path = ''
    save_path = ''

    def __init__(self, bin_path, save_path="/tmp/"):
        self.bin_path = bin_path
        self.save_path = save_path
        return

    # https://github.com/ReFirmLabs/binwalk/blob/master/API.md
    # https://github.com/ReFirmLabs/binwalk/wiki/Scripting-With-the-Python-API
    def binwalk_extractor(self, need_to_extract=False):
        files = []

        try:
            for module in binwalk.scan(self.bin_path,
                                       signature=True,
                                       quiet=not (
                                               'DEBUG' in os.environ and bool(
                                           os.environ['DEBUG'])),
                                       extract=need_to_extract):
                for result in module.results:
                    files.append([result.offset, result.description])
        except ModuleNotFoundError:
            logger.error('Install binwalk first!')
            logger.error('git clone https://github.com/ReFirmLabs/binwalk/')
            return -1
        return {'Files': files}

    def check_dump(self, block_size, offset=0):
        # checking file exist
        try:
            f_input = open(self.bin_path, 'rb')
        except:
            # TODO: fix exception
            logger.error('File {} not found!'.format(self.bin_path))
            return 0
        filesize = f_input.tell()

        # checking filesize
        if (filesize - offset) % block_size != 0:
            logger.error(
                'Filesize {} doesn\'t divide by {} block size!'.format(filesize,
                                                                       block_size))
            return 0
        f_input.close()
        return 1

    def little_endian(self, block_size=4, out_filename='out.bin'):

        if not self.check_dump(block_size):
            return

        # checking file creation
        try:
            f_output = open(self.save_path + out_filename, 'wb')
        except:
            # TODO: fix exception
            logger.error(
                "Can'\t create {} file!".format(self.save_path + out_filename))
            return

        f_input = open(self.bin_path, 'rb')

        while True:
            buf = f_input.read(block_size)
            buf = buf[::-1]
            if buf:
                f_output.write(buf)
            else:
                break
        f_input.close()
        f_output.close()
        logger.info('Created {} file!'.format(self.save_path + out_filename))
        return

    def oob_ecc_remover(self, start_offset=0, data_size=60, oob_size=4,
                        out_filename='out.bin'):
        '''

        :param start_offset: Offcet of data file (in bytes)
        :param data_size: Needed data size in 1 block (bytes)
        :param oob_size: Needed OOB/ECC size in 1 block (bytes)
        :param out_filename: File output name
        :return:
        '''
        block_size = data_size + oob_size
        if not self.check_dump(block_size, offset=start_offset):
            return

        # checking file creation
        try:
            f_output = open(self.save_path + out_filename, 'wb')
        except:
            # TODO: fix exception
            logger.error(
                "Can'\t create {} file!".format(
                    self.save_path + out_filename))
            return

        f_input = open(self.bin_path, 'rb')

        if start_offset != 0:
            buf = f_input.read(start_offset)
            f_output.write(buf)

        while True:
            buf = f_input.read(block_size)
            buf = buf[:data_size]
            if buf:
                f_output.write(buf)
            else:
                break
        f_input.close()
        f_output.close()
        logger.info('Created {} file!'.format(self.save_path + out_filename))
        return

    def cpu_rec(self):
        if not os.path.isfile(self.bin_path):
            return
        logger.info("Starting to detect CPU from {}".format(self.bin_path))
        logger.info("Thanks to https://github.com/airbus-seclab/cpu_rec !")
        arch = which_arch(open(self.bin_path, 'rb').read())
        arch = isf_which_arch(self.bin_path, fast=False, dumpdir=False, verbose=0)
        return arch
